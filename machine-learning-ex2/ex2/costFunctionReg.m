function [J, grad] = costFunctionReg(theta, X, y, lambda)
%COSTFUNCTIONREG Compute cost and gradient for logistic regression with regularization
%   J = COSTFUNCTIONREG(theta, X, y, lambda) computes the cost of using
%   theta as the parameter for regularized logistic regression and the
%   gradient of the cost w.r.t. to the parameters. 

% Initialize some useful values
m = length(y); % number of training examples

% You need to return the following variables correctly 
J = 0;
grad = zeros(size(theta));

% ====================== YOUR CODE HERE ======================
% Instructions: Compute the cost of a particular choice of theta.
%               You should set J to the cost.
%               Compute the partial derivatives and set grad to the partial
%               derivatives of the cost w.r.t. each parameter in theta
disp(size(theta))
h = sigmoid(X*theta)
J = (1/m)*sum((-1)*y'*log(h) - (1-y)'*log(1-h)) + (lambda/(2*m))*sumsq(theta(2:end))
disp("Size of h is"), disp(size(h))
disp("Size of y is"), disp(size(y))
disp("Size of X is"), disp(size(X))
disp("Size of theta is"), disp(size(theta))
grad(1,1:end) = (1/m)*sum((h-y).*X(1:end,1))
disp("Theta(2:end) size"),disp(size(theta(2:end)))
disp("Lambda*m*theta"), disp(lambda*m*theta(2:end))
grad(2:end,1:end) = (1/m)*sum((h-y).*X(1:end,2:end)) + (lambda/m)*theta'(2:end)
disp("other grad"),disp(size(grad(2:end,1:end)))





% =============================================================

end
