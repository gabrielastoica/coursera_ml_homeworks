function p = predict(Theta1, Theta2, X)
%PREDICT Predict the label of an input given a trained neural network
%   p = PREDICT(Theta1, Theta2, X) outputs the predicted label of X given the
%   trained weights of a neural network (Theta1, Theta2)

% Useful values
m = size(X, 1);
num_labels = size(Theta2, 1);

% You need to return the following variables correctly 
p = zeros(size(X, 1), 1);

% ====================== YOUR CODE HERE ======================
% Instructions: Complete the following code to make predictions using
%               your learned neural network. You should set p to a 
%               vector containing labels between 1 to num_labels.
%
% Hint: The max function might come in useful. In particular, the max
%       function can also return the index of the max element, for more
%       information see 'help max'. If your examples are in rows, then, you
%       can use max(A, [], 2) to obtain the max for each row.
%

disp("Number of labels"), disp(num_labels)
disp("Initial size of p"), disp(size(p))
disp("Size of theta1"), disp(size(Theta1))
disp("Size of theta2"), disp(size(Theta2))

X = [ones(m, 1) X];
z_1 = X*Theta1';
disp("z_1"), disp(size(z_1));
a_1 = sigmoid(z_1);
disp("a_1"),disp(size(a_1));
a_1 = [ones(size(a_1,1),1) a_1];
z_2 = a_1*Theta2';
disp("z_2"), disp(size(z_2));
a_2 = sigmoid(z_2);
disp("a_2"), disp(size(a_2));
[i,p] = max(a_2,[],2);





% =========================================================================


end
